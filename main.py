from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import f1_score

import random

import sqlite3

import numpy

with sqlite3.connect('comments.db') as db:

    f1_scores = list()
    for attempt in range(1, 100):

        shuffled_samples = db.execute("SELECT * FROM Comment").fetchall()

        random.seed(attempt)
        random.shuffle(shuffled_samples)

        training_ratio = int(len(shuffled_samples) * 0.75)

        training_data = [ x for x in map(lambda s: s[0], shuffled_samples[0:training_ratio])]
        training_target = list(map(lambda s: s[1], shuffled_samples[0:training_ratio]))

        tests = shuffled_samples[training_ratio:]

        count_vectorizer = CountVectorizer()
        training_counts = count_vectorizer.fit_transform(training_data)

        tfidf_transformer = TfidfTransformer()
        training_tfidf = tfidf_transformer.fit_transform(training_counts)

        classifier = SGDClassifier().fit(training_tfidf, training_target)

        predictions = []
        ground_truth = list(map(lambda s: s[1], tests))

        for test in tests:
            new_counts = count_vectorizer.transform([test[0]])
            X_new_tfidf = tfidf_transformer.transform(new_counts)
            result = classifier.predict(X_new_tfidf)
            predictions.append(classifier.predict(X_new_tfidf))

        f1_scores.append(f1_score(ground_truth, predictions, average='micro'))

    print(numpy.mean(f1_scores))
